const { Router } = require("express");
const UserService = require("../services/userService");
const {
  createUserValid,
  updateUserValid,
} = require("../middlewares/user.validation.middleware");
const { responseMiddleware } = require("../middlewares/response.middleware");

const router = Router();

const get = (req, res, next) => {
  const { error } = res.locals;
  try {
    if (!error) {
      const { id } = req.params;
      const user = UserService.search({ id });
      res.locals.result = user;
    }
  } catch (err) {
    res.locals = {
      ...res.locals,
      status: 404,
      message: err.message,
      error: true,
    };
  } finally {
    next();
  }
};

const getAll = (req, res, next) => {
  const { error } = res.locals;
  try {
    if (!error) {
      const users = UserService.allUsers();
      res.locals.result = users;
    }
  } catch (err) {
    res.locals = {
      ...res.locals,
      status: 404,
      message: err.message,
      error: true,
    };
  } finally {
    next();
  }
};

const post = (req, res, next) => {
  const { error } = res.locals;
  try {
    if (!error) {
      const { firstName, lastName, email, phoneNumber, password } = req.body;
      res.locals.result = UserService.createUser({
        firstName,
        lastName,
        email,
        phoneNumber,
        password,
      });
    }
  } catch (err) {
    res.locals = {
      ...res.locals,
      status: 404,
      message: err.message,
      error: true,
    };
  } finally {
    next();
  }
};

const put = (req, res, next) => {
  const { error } = res.locals;
  try {
    if (!error) {
      const { id } = req.params;
      const { firstName, lastName, email, phoneNumber, password } = req.body;

      res.locals.result = UserService.update(id, {
        firstName,
        lastName,
        email,
        phoneNumber,
        password,
      });
    }
  } catch (err) {
    res.locals = {
      ...res.locals,
      status: 404,
      message: err.message,
      error: true,
    };
  } finally {
    next();
  }
};

const remove = (req, res, next) => {
  const { error } = res.locals;
  try {
    if (!error) {
      const { id } = req.params;
      const user = UserService.deleteUser(id);
      res.locals.result = user;
    }
  } catch (err) {
    res.locals = {
      ...res.locals,
      status: 404,
      message: err.message,
      error: true,
    };
  } finally {
    next();
  }
};

const notFound = (req, res, next) => {
  res.locals.notFound = true;
  next();
};

router.get("/", getAll, responseMiddleware);
router.get("/:id", get, responseMiddleware);

router.post("/", createUserValid, post, responseMiddleware);
router.put("/:id", updateUserValid, put, responseMiddleware);
router.delete("/:id", remove, responseMiddleware);
router.all("*", notFound, responseMiddleware);

module.exports = router;
