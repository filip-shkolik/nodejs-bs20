const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

const get = (req, res, next) =>{
    const { error } = res.locals;

    try {
        if(!error){
            const { id } = req.params;
            const user = FighterService.search({ id });
            res.locals.result = user;
        }
    } catch(err){
        res.locals = {
            ...res.locals,
            status: 404,
            message: err.message,
            error: true
        }
    } finally {
        next();
    }
}

const getAll = (req, res, next) =>{
    const { error } = res.locals;
    try {
        if (!error) {
            const fighters = FighterService.allFighters();
            res.locals.result = fighters;
        }
    } catch(err) {
        res.locals = {
            ...res.locals,
            status: 404,
            message: err.message,
            error: true
        }
    } finally {
        next();
    }
}

const post = (req, res, next) =>{
    const { error } = res.locals;
    try {
        if(!error){
            const { name, health, power, defense } = req.body;
            res.locals.result = FighterService.createFighter({ name, health, power, defense });
        }
    } catch(err) {
        res.locals = {
            ...res.locals,
            message: err.message,
            error: true
        }
    } finally {
        next();
    }
}

const put = (req, res, next) =>{
    const { error } = res.locals;
    try {
        if(!error){
            const { id } = req.params;
            const { name, health, power, defense } = req.body;
            res.locals.result = FighterService.update(id, { name, health, power, defense });
        }
    } catch(err) {
        res.locals = {
            ...res.locals,
            status: 404,
            message: err.message,
            error: true
        }
    } finally{
        next();
    }
}

const remove = (req, res, next) =>{
    const { error } = res.locals;
    try {
        if(!error){
            const { id } = req.params;
            const fighter = FighterService.deleteFighter(id);
            res.locals.result = fighter;
        }
    } catch(err) {
        res.locals = {
            ...res.locals,
            status: 404,
            message: err.message,
            error: true
        }
    } finally {
        next();
    }
}



const notFound = (req,res,next) => {
    res.locals.wrongRoute = true;
    next()
};

router.get('/', getAll, responseMiddleware);
router.get('/:id', get, responseMiddleware);
router.post('/', createFighterValid, post, responseMiddleware);
router.put('/:id', updateFighterValid, put, responseMiddleware);
router.delete('/:id', remove, responseMiddleware);
router.all('*', notFound , responseMiddleware);

module.exports = router;