const { Router } = require('express');
const AuthService = require('../services/authService');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.post('/login', (req, res, next) => {
    try {
        res.locals.result = AuthService.login(req.body)
    } catch (err) {
        res.locals = {
            ...res.locals,
            error: true,
            message: err.message,
        }
    } finally {
        next();
    }
}, responseMiddleware);

router.all('*', (req,res,next) => {
    res.locals.notFound = true;
    next();
}, responseMiddleware)

module.exports = router;