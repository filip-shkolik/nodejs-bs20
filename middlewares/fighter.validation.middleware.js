const { fighter } = require('../models/fighter');

function validator({ id, name, health, power, defense }){
    if (id !== undefined) {
        return {
            error: true,
            message: 'Id is not permited to be in the body',
        }
    } 
    if (!name || !(/^.+$/.test(name))) {
        return {
            error: true,
            message: 'name is incorrect',
        }
    }
    if (!health || health < 9 || health > 100) {
        return {
            error: true,
            message: 'health is incorrect',
        }
    }
    if (!power || power < 0 || power > 100) {
        return {
            error: true,
            message: 'power is incorrect',
        }
    }
    if (!defense || defense < 0 || defense > 15) {
        return {
            error: true,
            message: 'defense is incorrect',
        }
    }
    
    return null;
}

const createFighterValid = (req, res, next) => {
    
    // Implement validatior for fighter entity during creation
    const result = validator(req.body, res, next);
    if (res) {
        res.locals = {
            ...res.locals,
            ...result,
        }
    }
    next();
}

const updateFighterValid = (req, res, next) => {

    // Implement validatior for fighter entity during update
    const result = validator(req.body, res, next);
    if (res) {
        res.locals = {
            ...res.locals,
            ...result,
        }
    }
    next();
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;