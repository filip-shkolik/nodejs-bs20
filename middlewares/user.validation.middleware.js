const { user } = require('../models/user');

function validator({ id, firstName, lastName, email, phoneNumber, password }){
    if (id !== undefined) {
        return {
            error: true,
            message: 'Id is not permited to be in the body',
        }
    } 
    if (!firstName || !(/^.+$/.test(firstName))) {
        return {
            error: true,
            message: 'firstName is incorrect',
        }
    }
    if (!lastName || !(/^.+$/.test(lastName))) {
        return {
            error: true,
            message: 'lastName is incorrect',
        }
    }
    if (!email || !(/^[a-zA-Z0-9_.-]+@gmail\.com$/.test(email))) {
        return {
            error: true,
            message: 'email is incorrect',
        }
    }
    if (!phoneNumber || !(/^\+380[0-9]{9}$/.test(phoneNumber))) {
        return {
            error: true,
            message: 'phoneNumber is incorrect',
        }
    }
    if (!password || !(/^.{3,}$/.test(password))) {
        return {
            error: true,
            message: 'password is incorrect',
        }
    }
    
    return null;
}

const createUserValid = (req, res, next) => {

    // Validatior for user entity during creation
    const result = validator(req.body, res, next);
    if (res) {
        res.locals = {
            ...res.locals,
            ...result,
        }
    }
    next();
}

const updateUserValid = (req, res, next) => {

    // Validatior for user entity during update
    const result = validator(req.body, res, next);
    if (res) {
        res.locals = {
            ...res.locals,
            ...result,
        }
    }
    next();
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;