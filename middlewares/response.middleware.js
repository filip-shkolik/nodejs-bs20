
const responseMiddleware = (req, res, next) => {
    const { notFound, error, message, result, status } = res.locals;

    if(notFound) {
        res.status(400).json({
            error: true,
            message: `Not found`,
        })
    }
    else if (error) {
        res.status(status || 400).json({
            error,
            message,
        })
    }  else {
        res.status(200).json(result)
    }
    next();
}

exports.responseMiddleware = responseMiddleware;