const { UserRepository } = require('../repositories/userRepository');

class UserService {

    createUser(userData) {
        const userWithEmail = UserRepository.getOne({ email: userData.email });
        const userWithPhone = UserRepository.getOne({ phoneNumber: userData.phoneNumber });
        if (userWithEmail || userWithPhone) {
            throw Error('User with such email or phone has already been created');
        }
        return UserRepository.create(userData);
    }

    updateUser(id, userData){
        const user = UserRepository.getOne( {id} );
        if(!user){
            throw Error('User not found. Can`t update')
        }
        return UserRepository.update(id,userData);
    }

    deleteUser(id){
        const user = UserRepository.getOne( {id} )
        if(!user){
            throw Error('User not found. Can`t delete')
        }
        UserRepository.delete(id);
        return user;
    }


    allUsers(){
        const users = UserRepository.getAll();
        if(!users || !users.length){
            return [];
        }
        return users;
    }

    search(search) {
        const item = UserRepository.getOne(search);
        if(!item) {
            throw Error('User not found. Can`t search')
        }
        return item;
    }
}

module.exports = new UserService();