const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {

    createFighter(fighterData) {
        const fighter = FighterRepository.getOne( { name: fighterData.name } );
        if (fighter) {
            throw new Error('Fighter with such a name has been already created ')
        }
        return FighterRepository.create(fighterData);
    }

    updateFighter(id, fighterData){
        const fighter = FighterRepository.getOne( {id} );
        if(!fighter){
            throw new Error('Fighter not found. Can`t update ')
        }
        return FighterRepository.update(id,fighterData);
    }

    deleteFighter(id){
        const fighter = FighterRepository.getOne( {id} )
        if(!fighter){
            throw new Error('Fighter not found. Can`t delete')
        }
        FighterRepository.delete(id)
        return fighter;
    }


    allFighters(){
        const fighters = FighterRepository.getAll();
        if(!fighters || !fighters.length){
            return [];
        }
        return fighters;
    }
    search(search){
        const item = FighterRepository.getOne(search);
        if(item){
            throw Error(`Fighter not found. Can't search`)
        }
        return item;
    }
}

module.exports = new FighterService();